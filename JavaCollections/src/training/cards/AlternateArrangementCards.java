package training.cards;

import training.cards.pojos.Card;

import java.util.Comparator;

public class AlternateArrangementCards implements Comparator<Card> {

    @Override
    public int compare(Card o1, Card o2) {
        if(o1.getSuite().getNumber() > o2.getSuite().getNumber()) {
            return -1;
        } else if(o1.getSuite().getNumber() < o2.getSuite().getNumber()) {
            return 1;
        } else {
            return (o1.getNumber().getWeight() < o2.getNumber().getWeight())? 1: -1;
        }
    }
}
