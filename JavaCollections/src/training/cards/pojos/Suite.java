package training.cards.pojos;

import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

public enum Suite {
    SPADE(1, "S"), HEART(2, "H"), DIAMOND(3, "D"), CLUB(4,"C");

    private int number;
    private String shortSymbol;

    private Suite(int number, String shortSymbol) {
        this.number = number;
        this.shortSymbol = shortSymbol;
    }

    public int getNumber() {
        return number;
    }

    public String getShortSymbol() {
        return shortSymbol;
    }

    public Suite getSuite(int number) {
        Set<Suite> suites = EnumSet.allOf(Suite.class);
        Optional<Suite> first = suites.stream().filter(suite -> {
            return suite.getNumber() == number;
        }).findFirst();
        return first.get();
    }

}
