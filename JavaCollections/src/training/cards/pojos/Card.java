package training.cards.pojos;

import java.util.Objects;

public class Card implements Comparable<Card> {
    private Suite suite;
    private CardNumber number;

    public Card(Suite suite, CardNumber number) {
        this.suite = suite;
        this.number = number;
    }

    public Suite getSuite() {
        return suite;
    }


    public CardNumber getNumber() {
        return number;
    }


    @Override
    public String toString() {
        return suite.getShortSymbol()+number.getFace();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(getSuite(), card.getSuite()) &&
                Objects.equals(getNumber(), card.getNumber());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getSuite(), getNumber());
    }

    @Override
    public int compareTo(Card o) {
        if(this.getSuite().getNumber() > o.getSuite().getNumber()) {
            return 1;
        } else if(this.getSuite().getNumber() < o.getSuite().getNumber()) {
            return -1;
        } else {
            return (this.getNumber().getWeight() < o.getNumber().getWeight())? -1: 1;
        }
    }
}
