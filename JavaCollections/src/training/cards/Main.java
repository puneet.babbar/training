package training.cards;

import training.cards.pojos.Card;
import training.cards.pojos.CardNumber;
import training.cards.pojos.Suite;

import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        consoleOutput();

        List<Card> cards = createCards();

        Scanner scanner = new Scanner(System.in);
        // Reading data using readLine
        int input = 0;
        boolean exit = false;
        CardOperations operations = new CardOperations();
        while (!exit) {
            input = scanner.nextInt();
            if (input == 5) {
                exit = true;
                continue;
            }
            switch (input) {
                case 1:
                    operations.shuffle(cards);
                    break;
                case 2:
                    System.out.println("Enter Suite - SPADE(1), HEART(2), DIAMOND(3), CLUB(4)");
                    int suiteNumber = scanner.nextInt();
                    operations.shuffleSuite(cards, suiteNumber);
                    break;
                case 3:
                    System.out.println("Enter Suite - SPADE(1), HEART(2), DIAMOND(3), CLUB(4)");
                    suiteNumber = scanner.nextInt();
                    operations.sortSuite(cards, suiteNumber);
                    break;
                case 4:
                    operations.sortCards(cards);
                    break;
                case 6:
                    operations.print(cards);
                    break;
            }
            consoleOutput();
        }

        System.out.println("Exiting");
    }

    private static void consoleOutput() {
        System.out.println("-------Cards--------");
        System.out.println("1. Shuffle All cards");
        System.out.println("2. Shuffle Suite of cards");
        System.out.println("3. Sort Suite of cards");
        System.out.println("4. Sort All cards according to weightage");
        System.out.println("5. Exit");
    }

    private static List<Card> createCards() {
        List<Card> cards = new ArrayList<>();
        Set<Suite> suites = EnumSet.allOf(Suite.class);
        Set<CardNumber> cardNumbers = EnumSet.allOf(CardNumber.class);
        suites.forEach(suite -> {
            cardNumbers.forEach(cardNumber -> { cards.add(new Card(suite, cardNumber));});
        });
        return cards;
    }

}
