package training.alternateimpl.pojos;

public enum CardNumber {
    CARD_A("A", 1),
    CARD_2("2", 2),
    CARD_3("3", 3),
    CARD_4("4", 4),
    CARD_5("5", 5),
    CARD_6("6", 6),
    CARD_7("7", 7),
    CARD_8("8", 8),
    CARD_9("9", 9),
    CARD_10("10", 10),
    CARD_J("J", 11),
    CARD_Q("Q", 12),
    CARD_K("K", 13);

    private int weight;
    private String face;

    private CardNumber(String face, int weight) {
        this.weight = weight;
        this.face = face;
    }

    public int getWeight() {
        return weight;
    }

    public String getFace() {
        return face;
    }
}
