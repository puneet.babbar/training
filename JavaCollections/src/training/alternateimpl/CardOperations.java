package training.alternateimpl;

import training.cards.AlternateArrangementCards;
import training.cards.pojos.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardOperations {
    public void shuffle(List<Card> cards) {
        Collections.shuffle(cards);
        cards.forEach(card -> System.out.println(card));
    }

    public void shuffleSuite(List<Card> cards, int suiteNumber) {
        List<Card> cardList = getSuiteCards(cards, suiteNumber);
        Collections.shuffle(cardList);
        cardList.forEach(card -> System.out.println(card));
    }

    public void sortSuite(List<Card> cards, int suiteNumber) {
        List<Card> cardList = getSuiteCards(cards, suiteNumber);
        cards.removeAll(cardList);
        Collections.sort(cardList);
        cards.addAll(cardList);
        cardList.forEach(card -> System.out.println(card));
    }

    public void sortCards(List<Card> cards) {
//        Collections.sort(cards);
        Collections.sort(cards, new AlternateArrangementCards());
        cards.forEach(card -> System.out.println(card));
    }

    private List<Card> getSuiteCards(List<Card> cards, int suiteNumber) {
//        List<Card> cardList = cards.stream().filter(card -> {
//            return card.getSuite().getNumber() == suiteNumber;
//        }).collect(Collectors.toList());
        List<Card> suiteCards = new ArrayList<>();
        for(Card card : cards) {
            if(card.getSuite().getNumber() == suiteNumber) {
                suiteCards.add(card);
            }
        }

        return suiteCards;
    }

    public void print(List<Card> cards) {
        cards.forEach(card -> System.out.println(card));
    }
}
