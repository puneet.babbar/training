package training.set;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class SetDemo {
    public static void main(String[] args) {
        String s1 = "This demo is about using a HashSet";
        Set<String> stringSet = new LinkedHashSet<>();
        stringSet.add(s1);
        stringSet.add("First");
        stringSet.add("Second");
        stringSet.add("Third");
        stringSet.add("First");
        stringSet.forEach(s -> System.out.println(s));
        System.out.println("Does the set contain the object? - "+stringSet.contains("Second"));
    }
}
