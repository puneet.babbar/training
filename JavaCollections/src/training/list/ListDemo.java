package training.list;

import java.util.*;

public class ListDemo {
    public static void main(String[] args) {
//        demoStringList();
        demoRandomIntList();
    }

    private static void demoRandomIntList() {
        //holder for random Integers
        List<Integer> randomList = new LinkedList<>();

        Random random = new Random(20);

        for(int i = 0; i < 20 ; i ++ ) {
            //adding random integers
            randomList.add(random.nextInt());
        }

        randomList.forEach(r -> System.out.println(r));
    }

    private static void demoStringList() {
        String s1 = "This demo is about using the Collection Interface";

        String[] strings = s1.split(" ");
        //getting an instance of List
        List<String> stringList = Arrays.asList(strings);

        String collection = "Collection";

        //check if an object exists in the list
        if( stringList.contains(collection)) {
            //get the index of a specific object
            int pos = stringList.indexOf(collection);
            //set an object at a specific location, and get the access to the old one.
            String oldObj = stringList.set(pos, "List");
            System.out.println("Discarding old object - "+oldObj);
        }
        //What happens if you try and add something to this list
//        stringList.add(".");
        stringList.forEach(s -> System.out.println(s));
    }
}
