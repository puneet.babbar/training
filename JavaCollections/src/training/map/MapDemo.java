package training.map;

import training.pojos.Score;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class MapDemo {
    public static void main(String[] args) {
        //Notice the output when you use HashMap vs LinkedHashMap
        Map<String, Score> studentScores = new HashMap<>();
//        Map<String, Score> studentScores = new LinkedHashMap<>();
        studentScores.put("Student1", new Score(98, 89));
        studentScores.put("Student2", new Score(76, 90));
        studentScores.put("Student3", new Score(80, 22));
        studentScores.put("Student4", new Score(90, 34));
        studentScores.put("Student5", new Score(12, 89));
        studentScores.put("Student6", new Score(45, 67));

        studentScores.entrySet().forEach(stringScoreEntry -> System.out.println(stringScoreEntry.getKey()+" score  - " + stringScoreEntry.getValue()));

        Set<String> strings = studentScores.keySet();
        for(String s: strings) {
            Score score = studentScores.get(s);
            System.out.println(score);
        }
    }
}
