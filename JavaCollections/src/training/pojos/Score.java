package training.pojos;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Score {
    private Integer SubjectA;
    private Integer SubjectB;

    public Score(Integer subjectA, Integer subjectB) {
        SubjectA = subjectA;
        SubjectB = subjectB;
    }

    @Override
    public String toString() {
        return "Score{" +
                "SubjectA=" + SubjectA +
                ", SubjectB=" + SubjectB +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return Objects.equals(SubjectA, score.SubjectA) &&
                Objects.equals(SubjectB, score.SubjectB);
    }

    @Override
    public int hashCode() {
        int hash = Objects.hash(SubjectA, SubjectB);
        System.out.println(hash);
        return hash;
    }

    public static void main(String[] args) {
        Set<Score> s = new HashSet<>();
        Score e = new Score(21, 33);
        s.add(e);
        s.contains(e);
    }
}
