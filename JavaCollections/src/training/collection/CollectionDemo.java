package training.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class CollectionDemo {
    public static void main(String[] args) {
        //ArrayList is a kind of List, and Collection is the base interface for all collections.
        Collection<String> collection = new ArrayList<>();
        collection.add("hello");
        collection.add("world");
        System.out.println("Print List");

        //Iterate and print all elements of the collection (underlying implementation is ArrayList
        collection.forEach(s -> System.out.println(s));

        //reassigning the collection to a set
        collection = new HashSet<>();
        collection.add("foo");
        collection.add("bar");
        System.out.println("Print Set");

        //Iterate and print all elements of the collection (underlying implementation is HashSet
        collection.forEach(s -> System.out.println(s));

    }
}
